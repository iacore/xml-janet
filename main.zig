// Copyright (C) 2021-2023 Chadwain Holness
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const std = @import("std");
const rem = @import("rem");
const util = @import("util.zig");

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer std.debug.assert(gpa.deinit() == .ok);
    const allocator = gpa.allocator();

    // This is the text that will be read by the parser.
    // Since the parser accepts Unicode codepoints, the text must be decoded before it can be used.
    const input = "<!doctype html><html><p><a class=link href=#main>tag soup</p ></a><b>hi</b>";
    const decoded_input = &util.utf8DecodeStringComptime(input);

    // Create the DOM in which the parsed Document will be created.
    var dom = rem.Dom{ .allocator = allocator };
    defer dom.deinit();

    // Create the HTML parser.
    var parser = try rem.Parser.init(&dom, decoded_input, allocator, .report, false);
    defer parser.deinit();

    // This causes the parser to read the input and produce a Document.
    try parser.run();

    // `errors` returns the list of parse errors that were encountered while parsing.
    // Since we know that our input was well-formed HTML, we expect there to be 0 parse errors.
    // const errors = parser.errors();
    // std.debug.assert(errors.len == 0);

    // We can now print the resulting Document to the console.
    const stdout = std.io.getStdOut().writer();
    const document = parser.getDocument();
    try printDocument(stdout, document, &dom, allocator);
}

pub fn printDocument(writer: anytype, document: *const rem.Dom.Document, dom: *const rem.Dom, allocator: std.mem.Allocator) !void {
    // try std.fmt.format(writer, "Document: {s}\n", .{@tagName(document.quirks_mode)});

    // try util.printDocumentCdatas(writer, document, 0);

    // if (document.doctype) |doctype| {
    //     try std.fmt.format(writer, "  DocumentType: name={s} publicId={s} systemId={s}\n", .{ doctype.name, doctype.publicId, doctype.systemId });
    // }

    // try util.printDocumentCdatas(writer, document, 1);

    const ConstElementOrCharacterData = rem.Dom.ElementOrCharacterData;

    var unvisited = std.ArrayList([]const ConstElementOrCharacterData).init(allocator);
    defer unvisited.deinit();

    if (document.element) |document_element| {
        try unvisited.append(&.{.{ .element = document_element }});
    }

    while (unvisited.popOrNull()) |top| {
        if (top.len == 0) continue;
        const node = top[0];
        const depth = unvisited.items.len;
        try unvisited.append(top[1..]);
        for (0..depth) |_| {
            try std.fmt.format(writer, "  ", .{});
        }

        switch (node) {
            .element => |element| {
                try std.fmt.format(writer, "Element: local_name={s} namespace={s} attributes=[", .{ element.localName(dom), @tagName(element.namespace()) });
                const num_attributes = element.numAttributes();
                {
                    try writer.writeAll(" ");
                    const attribute_slice = element.attributes.slice();
                    var i: u32 = 0;
                    while (i < num_attributes) : (i += 1) {
                        const key = attribute_slice.items(.key)[i];
                        const value = attribute_slice.items(.value)[i];
                        if (key.prefix == .none) {
                            try std.fmt.format(writer, "\"{s}\"=\"{s}\" ", .{ key.local_name, value });
                        } else {
                            try std.fmt.format(writer, "\"{s}:{s}\"=\"{s}\" ", .{ @tagName(key.prefix), key.local_name, value });
                        }
                    }
                }
                try std.fmt.format(writer, "]\n", .{});

                // Add children to stack
                try unvisited.append(element.children.items);
            },
            .cdata => |cdata| try util.printCdata(writer, cdata),
        }
    }

    // try util.printDocumentCdatas(writer, document, 2);
}
